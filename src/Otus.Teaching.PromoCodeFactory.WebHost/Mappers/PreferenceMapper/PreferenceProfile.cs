using System.Collections.Generic;
using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers.PreferenceMapper
{
    public class PreferenceProfile: Profile
    {
        public PreferenceProfile()
        {
            CreateMap<Preference, PreferenceResponse>()
                .ForMember(dst => dst.Customers,
                    opt => opt.Ignore());
            
            CreateMap<IEnumerable<CustomerShortResponse>, PreferenceResponse>()
                .ForMember(dst => dst.Customers,
                    opt =>
                        opt.MapFrom(src => new List<CustomerShortResponse>(src)));

            CreateMap<Preference, PreferenceShortResponse>();
        }
    }
}