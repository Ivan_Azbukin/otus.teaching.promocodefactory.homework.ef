using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers.PromoCodeMapper
{
    public class PromocodeProfile: Profile
    {
        public PromocodeProfile()
        {
            CreateMap<PromoCode, PromoCodeShortResponse>();
            
            CreateMap<GivePromoCodeRequest, PromoCode>()
                .ForMember(dest => dest.Code,
                    opt =>
                        opt.MapFrom(src => src.PromoCode))
                .ForMember(dst => dst.Customer,
                    opt => opt.Ignore())
                .ForMember(dst => dst.Preference,
                    opt => opt.Ignore());
            
            CreateMap<Customer, PromoCode>()
                .ForMember(dst => dst.Customer,
                    opt =>
                        opt.MapFrom(src => src))
                .ForAllOtherMembers(opt => opt.Ignore());
            
            CreateMap<Preference, PromoCode>()
                .ForMember(dst => dst.Preference,
                    opt =>
                        opt.MapFrom(src => src))
                .ForAllOtherMembers(opt => opt.Ignore());
        }
    }
}